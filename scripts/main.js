$(document).ready(function () {
	$('.header__hamburger').click(function () {
		$('.main-wrapper').fadeOut(250);
		$('.main-wrapper').css({ display: 'none' });
		$('.hamburger-wrapper').fadeIn(250);
		$('.hamburger-wrapper').css({ display: 'flex' });
	});

	$('.close-hamburger').click(function () {
		$('.hamburger-wrapper').fadeOut(250);
		$('.hamburger-wrapper').css({ display: 'none' });
		$('.main-wrapper').fadeIn(250);
		$('.main-wrapper').css({ display: 'grid' });
	});
});
