new Glide('.glide', {
	type: 'carousel',
	startAt: 1,
	perView: 3,
	gap: 24,
	peek: {
		before: 108,
		after: 108,
	},
	breakpoints: {
		375: {
			startAt: 3,
			perView: 1,
			gap: 16,
			peek: {
				before: 29,
				after: 29,
			},
		},
	},
}).mount();
